""" @file           main.py 
    @brief          Main file for running an encoder display program
    @details        Runs an encoder task and user task which allow the user to collect positional data
                    from one or more encoders. The user is able to zero the position, display position and delta,
                    and collect data for up to 30 seconds.
                    PYTHON FILES LOCATION: https://bitbucket.org/kcchappe/me305lab/src/master/Lab%202/
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/12/2021

"""
#import statements
import pyb
import shares
import encoderTM
import task_encoder
import task_user


def main():
    """@brief    Runs main
    """
 
#    initialize encoders
#    initialize pins
    pinPC6 = pyb.Pin (pyb.Pin.cpu.C6)   #encoder 1, pinA.
    pinPC7 = pyb.Pin (pyb.Pin.cpu.C7)   #encoder 1, pinB.
    encoder_1 = encoderTM.Encoder(pinPC6, pinPC7, 3)  
    
#    #same for encoder 2?
    pinPB6 = pyb.Pin (pyb.Pin.cpu.B6)
    pinPB7 = pyb.Pin (pyb.Pin.cpu.B7)
    encoder_2 = encoderTM.Encoder(pinPB6, pinPB7, 4)       
   
 #Initialize motors
     #motor 1
    pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
    pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
    #create motor obj
    
    #motor 2
    pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
    pinB1 = pyb.Pin(pyb.Pin.cpu.B1)
    #create motor object

    pinFault = pyb.Pin(pyb.Pin.cpu.B2) 
    fault_pin = shares.Share(False) 
      
    #initialize shares for encoder 1
    position_1 = shares.Share(0)
    delta_1 = shares.Share(0)
    z_flag_1 = shares.Share(False)
    
    #initialize shares for encoder 2
    position_2 = shares.Share(0)
    delta_2 = shares.Share(0)
    z_flag_2 = shares.Share(False)
    
    pinSleep = pyb.Pin(pyb.Pin.cpu.A15)
    #fault flag (share obj)
    
    
    #intitialize periods
    
    encoder_per = 100
    period_data = 50_000        #changed this  for overflow error - KC
    period_user = 1000
    
    #motor period (future)
    
    USER = task_user.Task_User(period_user, period_data, position_1, delta_1, z_flag_1, position_2, delta_2, z_flag_2)
        #user arguments (self, period_user, period_data, position_1, delta_1, z_flag_1, position_2, delta_2, z_flag_2)
        
    ENC1 = task_encoder.Task_Encoder(encoder_per, encoder_1, position_1, delta_1, z_flag_1)
        #task_encoder_1 arguments: (period, encoder1, pos1, del1, zflag1, name1 = NONE)         

    ENC2 = task_encoder.Task_Encoder(encoder_per, encoder_2, position_2, delta_2, z_flag_2)
        #task_encoder_2 arguments: (period, encoder2, pos2, del2, zflag2, name2 = NONE)

    #MOT1 = task_motor.Task_Motor(motor_per, motor_!, fault_flag)
    
    
    #list of all tasks to run
    #task_list = [USER, ENC1, ENC2]
    
    #run tasks
    while(True):
        try:
            
            
            ENC1.run()
            ENC2.run()
            USER.run()
#            for task in task_list:
#                task.run()
            
        except KeyboardInterrupt:
            break
    
    #happens after keyboard interrupt occurs
    print('Program Terminating')
    

if __name__ == '__main__':
    main()
