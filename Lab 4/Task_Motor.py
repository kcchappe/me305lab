""" @file           Task_Motor.py
    @brief          Motor task for utilizing Motor class
    @details        Implements FSM associated with utilizing custom Motor class.
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/12/2021

"""


#import statements
import utime
#from micropython import const
#import motor_driver #should this be here?
#import closedloop

class Task_Motor():
    ''' @brief      Motor task for controlling Motor class
        @details    Implements a finite state machine (to do what?) <---------------
    '''
    def __init__(self, motorID,  DC_val, faultFlagShare, period, motorDriver):
        ''' @brief              Constructs an Motor task.
            @details            The Motor task is implemented as a finite state machine. <-------- edit this to be unique
            @param              period   Period that the task runs at, in microseconds.
            @param              motor    Designates target Motor object 
            @param              DC_val_1   Value of duty change for motor
            @param              faultFlagShare 
         '''
        
        self.motorID = motorID
        self.DC_val = DC_val
        self.faultFlagShare = faultFlagShare 
        self.motorDriver = motorDriver
        self.currentMotor = self.motorDriver.motor(motorID)
        self.period = period
        self.nextTime = utime.ticks_add(utime.ticks_us(), self.period)
        
        #self.faultFlag = motor_driver.faultFlag                        #flag brought from DRV8847 to Task_Motor
        #self.faultFlagShare = faultFlagShare                    #flag shared between Task_User and Task_Motor

       
        #self.newDC = 0
        
        #self.next_time = utime.ticks_add(0, period)
       
        
    def run(self):
        ''' @brief      Runs the encoder task.
            @details    This will update the encoder data (position, time, delta).
        '''

         #check if time when run has passed the "next time" the task should run
        if utime.ticks_diff(utime.ticks_us(), self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            self.currentMotor.set_duty(self.DC_val.read())  
            if not self.faultFlagShare.read():
                self.motorDriver.enable()
            
#             if self.faultFlag == True:                          #if motor_driver raises flag
#                 print("Task_Motor sees a fault")
#                 self.faultFlagShare.write(True)                 #update shared flag (for task_user)
#             elif :
                
            
#             elif:
#                 self.newDC = self.DC_val.read()
#                 self.motor.set_duty(self.newDC)              # <---- DC_val is causing issues!!
# #                print(self.newDC)
