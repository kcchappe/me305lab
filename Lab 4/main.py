""" @file           main.py 
    @brief          Main file for running an encoder display program
    @details        Runs an encoder task and user task which allow the user to collect positional data
                    from one or more encoders. The user is able to zero the position, display position and delta,
                    and collect data for up to 30 seconds.
                    PYTHON FILES LOCATION: https://bitbucket.org/kcchappe/me305lab/src/master/Lab%202/
                    \image html Lab4Graph.png
                    
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/12/2021

"""
#import statements
#import pyb
import shares
#import encoderTM
import task_encoder
import task_user
import motor_driver
import task_motor
import task_controller
import closedloop

encoderPeriod = 1_000
userPeriod = 10_000
collectPeriod = 100_000
motorPeriod = 1_000
controllerPeriod = 1_000

# def main():
#     """@brief    Runs main
#     """
 
    # #initialize encoders
    # pinPC6 = pyb.Pin (pyb.Pin.cpu.C6)   #encoder 1, pinA.
    # pinPC7 = pyb.Pin (pyb.Pin.cpu.C7)   #encoder 1, pinB.
    # encoder_1 = encoderTM.Encoder(pinPC6, pinPC7, 3)  
    
    # pinPB6 = pyb.Pin (pyb.Pin.cpu.B6)   #encoder 2, pinA
    # pinPB7 = pyb.Pin (pyb.Pin.cpu.B7)   #encoder 2, pinB
    # encoder_2 = encoderTM.Encoder(pinPB6, pinPB7, 4)

if __name__ == '__main__':
    
    #initialize shares for encoder 1
    position_1 = shares.Share(0)
    delta_1 = shares.Share(0)
    z_flag_1 = shares.Share(False)
    DC_val_1 = shares.Share(0)
    o_meas_1 = shares.Share(0)
    o_ref_1 = shares.Share(0)
    data_1 = shares.Share()
    
    #initialize shares for encoder 2
    position_2 = shares.Share(0)
    delta_2 = shares.Share(0)
    z_flag_2 = shares.Share(False)
    DC_val_2 = shares.Share(0)
    o_meas_2 = shares.Share(0)
    o_ref_2 = shares.Share(0)
    data_2 = shares.Share()
    
    faultFlagShare = shares.Share(False)

    srQueue = shares.Queue()
    
    dataQueue = shares.Queue()
    
    motor_drv = motor_driver.DRV8847(faultFlagShare) 
    motor_drv.enable()
    
    closedLoop1 = closedloop.ClosedLoop(1, o_ref_1, o_meas_1, DC_val_1)
    
    closedLoop2 = closedloop.ClosedLoop(2, o_ref_2, o_meas_2, DC_val_2)
    
    #Initialize motors
    #motor 1
    # pinB4 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)     #motor 1, pinA  
    # pinB5 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)     #motor 1, pinB
    
    
    # #motor 2
    # pinB0 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)     #motor 2, pinA
    # pinB1 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)     #motor 2, pinB

    # pinSleep = pyb.Pin.cpu.A15
    # pinFault = pyb.Pin.cpu.B2

    #motor_drv = motor_driver.DRV8847(3, pinFault, pinSleep)
    # motor_1 = motor_drv.motor(1, 2, pinB4, pinB5)
    # motor_2 = motor_drv.motor(3, 4, pinB0, pinB1)
    
    # faultFlagShare = shares.Share(False)
    # DC_val_1 = shares.Share(0)
    # DC_val_2 = shares.Share(0)
    
    #intitialize periods
  
    
    
    USER = task_user.Task_User(userPeriod, collectPeriod, 
                               z_flag_1, z_flag_2, 
                               data_1, data_2, 
                               faultFlagShare, 
                               DC_val_1, DC_val_2, 
                               o_meas_1, o_meas_2, 
                               o_ref_1, o_ref_2,
                               closedLoop1, closedLoop2,
                               dataQueue, srQueue)
                               
                              
        #user arguments (self, period_user, period_data, position_1, delta_1, z_flag_1, position_2, delta_2, z_flag_2, faultFlagShare)
     
        
    CONT1 = task_controller.Task_Controller(controllerPeriod, closedLoop1)
    
    CONT2 = task_controller.Task_Controller(controllerPeriod, closedLoop2)
    
    
    ENC1 = task_encoder.Task_Encoder(encoderPeriod,  z_flag_1, data_1, o_ref_1, 1)
        #task_encoder_1 arguments: (period, encoder1, pos1, del1, zflag1, name1 = NONE)         

    ENC2 = task_encoder.Task_Encoder(encoderPeriod,  z_flag_2, data_2, o_ref_2, 2)
        #task_encoder_2 arguments: (period, encoder2, pos2, del2, zflag2, name2 = NONE)

    MOT1 = task_motor.Task_Motor(1, DC_val_1, faultFlagShare, motorPeriod, motor_drv)
        #task_motor_1 arguments: (period, motor1, DC_val, faultFlagShare)

    MOT2 = task_motor.Task_Motor(2, DC_val_2, faultFlagShare, motorPeriod, motor_drv)
        #task_motor_2 arguments: (period, motor2, DC_val, faultFlagShare)
        

    

    #list of all tasks to run
    #task_list = [USER, ENC1, ENC2]
 
    
    #USER.list_commands()
 
    #run tasks
    while(True):
        try:
            CONT1.run()
            CONT2.run()
            ENC1.run()
            ENC2.run()
            MOT1.run()
            MOT2.run()
            USER.run()
            
#            for task in task_list:
#                task.run()
            
        except KeyboardInterrupt:
            motor_drv.disable()
            print('Program Terminating')
            break
    #happens after keyboard interrupt occurs
  
