""" @file                       encoder.py
    @brief                      A driver for reading from Quadrature Encoders
    @details                    Includes the class Encoder which is used to create encoder objects.
    @author                     Tim Matista
    @author                     Kendall Chappell
    @date                       November 14, 2021
"""

import pyb

class Encoder:
    ''' @brief                  Interface with quadrature encoders
        @details                This class is a driver for quadrature encoders interfaced with a Nucleo.
                                Updates encoder position and change in encoder position. Detects and corrects under/over flow.
                                Manual position setting function included.
    '''
        
    def __init__(self, timNum, pinA, pinB):
        ''' @brief      Constructs encoder object
            @details    The Encoder object stores position and delta values and
                        provides methods to get position or delta and to set
                        position. The hardware timer is set up in this constructor.
            @param      timNum      Timer number used by Encoder object.
            @param      pinA        First encoder pin.
            @param      pinB        Second encoder pin.
        '''
        self.period = 2 ** 16 - 1
        self.timer = pyb.Timer(timNum,period=(self.period),prescaler=0)
        self.chanA = self.timer.channel(1,mode=pyb.Timer.ENC_AB,pin=pinA)
        self.chanB = self.timer.channel(2,mode=pyb.Timer.ENC_AB,pin=pinB)
        self.position = 0
        self.delta = 0
        self.prevTick = 0
        self.currentTick = 0
        
    def update(self):
        """ @brief      Updates encoder position and delta
            @details    Updates variables which store position values and 
                        change in position (delta) values. Detects and corrects under/over flow.
        """
        self.prevTick = self.currentTick
        self.currentTick = self.timer.counter()
        self.delta = self.currentTick - self.prevTick
        
        if self.delta >= self.period / 2:
            self.delta -= self.period
        elif self.delta <= -self.period / 2:
            self.delta += self.period
        
        self.position += self.delta
            
    def get_position(self):
        ''' @brief              Returns encoder position
            @details            Returns the encoder position, found by update().
                                Call update() before this function to get most recent position.
            @return             The position of the encoder shaft.
        '''
        return self.position
    
    def set_position(self, targetPosition):
        ''' @brief              Sets encoder position
            @details            Sets the encoder position to input value. 
                                Usually used to zero or "home" encoder.
            @param              targetPosition     The new position of the
                                                    encoder shaft.
        '''
        self.position = targetPosition
        
    def get_delta(self):
        ''' @brief              Returns encoder delta
            @details            Returns the encoder delta, found by update().
                                delta is the difference in ticks 
                                between the two most recent updates.
            @return             The change in position of the encoder shaft
                                between the two most recent updates.
        '''
        return self.delta
