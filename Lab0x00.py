# -*- coding: utf-8 -*-
"""@file.fibonacci.py
Created on Tue Sep 21 15:46:35 2021

@author: Kendall Chappell

"""
"""def fib(idx):
    
    
    return 0

if __name__== '__main__':
    
    
    idx = 0
    
    print ('Fibonacci number at  '
           'index {:} is {:}.' .format(idx,fib(idx)))
    """
def fib (idx):
    #These are the first two values of the Fibonacci sequence because they must be referenced
    n_0 = 0
    n_1 = 1
   
    #If statements are used for the first 2 index values because they have already been defined
    if idx == 0:
        n_current = 0
    if idx == 1:
        n_current = 1
          
    i=1
    #All values indices greater than 1 are found using a while loop that ends when the index is reached
    while i < idx:
        n_current = n_1 + n_0
        n_0 = n_1
        n_1 = n_current
        
        i = i+1
        #The result from the while loop is printed in the following string
    print('Fibonacci number at  '
          'index {:} is {:}.'.format(idx, n_current))
  
  
    
  #This stops the code from running when imported 
if __name__ == '__main__':

        #Check that the user input value is >=0 and an integer, if not, print an error message
    while True:
        #check that input value is an integer
        try:
            idx = int(input('Please enter a number: '))
        except ValueError:
            print('Value must be an integer')
            continue
        #check that input value is also positive
        if idx<0:
            print('Value must be positive')
        else: fib(idx)
        playAgain = input('Input another index? (y/n)')
        
        if playAgain == 'y':
            continue
        elif playAgain == 'n':
            break
        