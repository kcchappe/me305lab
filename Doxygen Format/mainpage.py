'''@file                mainpage.py
   @brief               Brief doc for mainpage.py
   @details             Detailed doc for mainpage.py 

   @mainpage

   @section sec_intro   Introduction
                        Some information about the whole project

   @section sec_motDriv     Motor Driver
                            Some information about the motor driver with links.
                            Please see motor_driver.Motor for details.

   @section sec_encDriv     Encoder Driver
                            Some information about the encoder driver with links. 
                            Please see encoder.Encoder for details.
                        
   @section sec_motTask     Motor Task
                            Some information about the motor task with links.
                            Please see Task_Motor.Task_Motor for details.

   @section sec_encTask    User Task
                           Some information about the user task with links.
                           Please see Task_User.Task_User for details.
                        
   @section sec_share       Shares
                            Some information about the shares file with links.
                            Please see shares.Share for details.

   @section sec_main        main
                            Some information about the main file with links.
                            Please see main for details.
                        
   @section sec_loop        Closed Loop
                            Some information about the closed loop task with links.
                            Please see closedLoop.ClosedLoop for details.

   @section sec_IMU         IMU
                            Some information about the IMU file with links.
                            Please see BNO055.BNO055 for details.
  
  @section sec_lab2         Lab 2
                            \image  html Lab2TaskDiagram.JPG width=800px
                            \image html Lab2TaskEncoderFSM.JPG width=800px
                            \image html Lab2TaskUserFSM.JPG width=800px
  
  @section sec_lab3         Lab 3
                            \image  html Lab3TaskDiagram.JPG width=800px
  
  @section sec_lab4         Lab 4
                            \image html Lab4Graph.png
                            \image html Lab4TaskUserFSM.JPG width=800px
  
  @section sec_HW           HW 0x02 and HW 0x03
   
   @author              Kendall Chappell

   @copyright           License Info

   @date                December 2, 2021
'''