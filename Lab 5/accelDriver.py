""" @file               accelDriver.py
    @brief              A file for the BNO055 IMU from Bosch. 
    @details            This file contains the class BNO055 and allows the user to manually calibrate the IMU upon start up. The user receives verifcation that the IMU has been calibrated, then 
                        the IMU will begin collecting data on roll, pitch, and heading. 
    @author             Kendall Chappell
    @author             Tim Matista
    @date               11/2/2021
"""


import pyb
import struct
#device address is 0x28

class BNO055:
    """ @brief      Class for the Bosch BNO055 IMU             
        @details    Allows for the IMU to have the operating mode set or read, read the calibration status, recognize when the calibration is complete
                    and read Euler angles (heading, pitch, and roll) in degrees. Allows for the angular velocities to be read. 
    """
    def __init__(self, master_object, devAd):
        """ @brief       Inititalizes IMU object       
            @details     Takes in the master object to communicate with the IMU.
            @param       master_object  The I2C controller
            @param       devAd The device address
        """           
        self.i2c = master_object
        self.eulerAd = 0x1A                     #0x1A - 0x1F  fpr heading, pitch, roll?
        self.modeAd = 0x3D                       #the address of the operation mode
        self.accAd = 0x28                        # OX28 - 0X2d?? for  x, y, z
        self.devAd = devAd
        
        print("i2c created")    #<---- this can be removed, only for testing
        
    def opMode(self, mode):
        """ @brief      Sets the IMU operating mode            
            @details    Sets the OPR_MODE register on the BNO055 
            @param mode         opperating mode
        """         
        #print("Setting Operating Mode") #<---- this can be removed, only for testing
        self.i2c.mem_write(mode, self.devAd, self.modeAd)
        #print(self.i2c.mem_read(1, self.devAd, self.modeAd)[0])   #<--- this can be removed, only for testing          
    
        
    def calStat(self):
        """ @brief     Gets the IMU calibration status         
            @details   Reads the value of the calibration status from the BNO055 register
        """                
        self.cal_statByte = bytearray([self.i2c.mem_read(1, self.devAd,35)[0]]) 
            #read the calibration status (1 byte at register 35) of the targeted device

        print("Binary calibration status:", '{:#010b}'.format(self.cal_statByte[0]))
            #print cal_statByte as a binary number

        self.cal_status =   (self.cal_statByte[0] & 0b11,                 
                            (self.cal_statByte[0] & 0b11 << 2) >> 2,
                            (self.cal_statByte[0] & 0b11 << 4) >> 4,
                            (self.cal_statByte[0] & 0b11 << 6) >> 6)         
        print("calibration status (as numbers)", self.cal_status)
        print('\n')
        
        #add logic for what to do if calibration is not complete
            #check back here to see if complete etc. 
            #dont trap in while loop
            
        if self.cal_status[3] == 3:
            if self.cal_status == (3,3,3,3): 
                print("calibration is complete - ish")
                self.calCoeff_b()
                #self.calSet(self.calCoeff_b()) <----- setup the calSet function before we do this!
                
            else:
                print("false positive")
        else:
            pass
#        elif self.cal_status[0] != 3:
#            print("mag needs calibrating") #what component is here?
#        elif self.cal_statByte[1] != 3:
#            print("component needs calibrating") #what component is here?
#        elif self.cal_statByte[2] != 3:
#            print("component needs calibrating") #what component is here?
#       
        
    
    def calCoeff_b(self):
        """ @brief      Gets the current calibration coefficients of the IMU           
            @details    Gets the value stored in the CALIB_STAT register on the BNO055
        """    
 
        self.coeffByte = bytearray(22)
        self.i2c.mem_read(self.coeffByte, self.devAd, 0x55)
        print(self.coeffByte)
        
    def calSet(self, coeffByte):
        """ @brief    Sets the calibration coefficients in the IMU 
            @details    Sets the values for the calibration coefficients of the IMU stored in 0x35 and 0x6A
            @param coeffByte        
        """    
        print("write calibration coeff from pre-recorded binary data")
        #self.i2c.mem_write(0,0,0)                          <------ fill this in
    
    def Euler(self):
        """ @brief      Reads the Euler angles (degrees)           
            @details    Returns a list of the Euler angles in the order (heading, roll, pitch)
        """    
        #print("read euler angles to use as state measurments")
          
        self.eul_bytes = self.i2c.mem_read(6, self.devAd, self.eulerAd)

            #heading_lsb, heading_msb, eul_roll_lsb, eul_roll_msb, eul_pitch_lsb, eul_pitch_msb
        self.heading = self.eul_bytes[0] | self.eul_bytes[1] << 8  # EUL_Heading
        self.roll    = self.eul_bytes[2] | self.eul_bytes[3] << 8  # EUL_Roll
        self.pitch   = self.eul_bytes[4] | self.eul_bytes[5] << 8  # EUL_Pitch
        
        #print("Unsigned:", (self.heading, self.roll, self.pitch))    
        
        if self.heading > 32767:
            self.heading -= 65536
        if self.roll > 32767:
            self.roll -= 65536
        if self.pitch > 32767:
            self.pitch -= 65536
            
        #print("Signed:", (self.heading, self.roll, self.pitch))
            
        #SCALE? SEE DATA SHEET
        self.heading /= 16
        self.roll /= 16
        self.pitch /= 16
        print("Scaled:", (self.heading, self.roll, self.pitch))
        print('\n')
        #return vs selfdot?
        
    def angVel(self, ARGUMENTS):
        """ @brief         Reads the angular velocity        
            @details      Returns the angle velocity  in the order (x, y, z)
        """    
        print("read angular velocity to use as state measurements")
        



#calState = bytearray(1)
#i2c.mem_read(calState, 0x28, 23) #23 is 35 in hex



#if calState == 0:
    #calibrate the device


#set op mode, then read from data register
#move device
#see change
#
#
#operating mode euler angles ang velocity
#then focus on calibration



#read about pyb.I2C
#master not controller

#do not use isRead or Scan functions

#page 50 has register map, this is helpful!

#practice reading 4 bytes of data from id registers
#make sure you are getting what you want from output data