""" @file           Task_User.py
    @brief          Encoder task for user input
    @details        Periodically check for user input related to encoder task
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/15/2021
"""

time = 0
pos = 1
delta = 2


#import statements
import utime
import pyb
from micropython import const

#designate states
    #add new state?
S0_init = const(0)
S1_wait = const(1)
S2_collect = const(2)
S3_print = const(3) 

class Task_User:
    """ @brief         Creates a user interface 
        @details       Allows the user to input commands through the keyboard
                       which will be interpretted by this file. 
    """
    
    def __init__(self, period_check, period_data, pos_1, del_1, z_flag_1, pos_2, del_2, z_flag_2):
        """ @brief      Initializes a user task.
            @param      period_check   Period, in microseconds, at which to check for new user commands
            @param      period_data    Period, in microseconds, at which to record encoder data
            @param      pos_1          Encoder 1 position
            @param      del_1          Encoder 1 delta
            @param      z_flag_1       Encoder 1 zero flag
            @param      pos_2          Encoder 2 position
            @param      del_2          Encoder 2 delta
            @param      z_flag_2       Encoder 2 zero flag
        """
#check that the argument system is as it should be

        self.ser = pyb.USB_VCP()



#check that initilization is as it should be
        self.period_check = period_check 
        self.period_data = period_data
        
        #initialize encoder 1 paramaters
        self.pos_1 = pos_1
        self.del_1 = del_1
        self.z_flag_1 = z_flag_1
 
        
        #initialize encoder 2 paramers
        self.pos_2 = pos_2
        self.del_2 = del_2
        self.z_flag_2 = z_flag_2
        



        self.runs = 0
        
        self.next_check = utime.ticks_add(utime.ticks_us(), self.period_check)
        self.next_data = utime.ticks_add(utime.ticks_us(), self.period_data)
        self.data = []
        
        self.dataLen = 30e6 / period_data + 1       #try making this a smaller number if receiving an overflow error 
        
        self.state = S0_init
        
    def run(self):
        """ @brief Runs one iteration of the FSM
        """
        #self.current_time = utime.ticks_us()
        #print("user is running") 
        

        
        if self.state == S0_init:
            print("Program initiated \n"
                 "z: Set encoder 1 position to zero  \n"
                 "Z: Set encoder 2 position to zero  \n"
                 "p: print encoder 1 position  \n"
                 "P: print encoder 2 position  \n"
                 "d: print encoder 1 delta to puTTY  \n"
                 "D: print encoder 2 delta to puTTY  \n"
                 "g: collect encoder 1 data for 30 seconds, then print to puTTY   \n"
                 "G: collect encoder 2 data for 30 seconds, then print to puTTY   \n")
            self.transition_to(S1_wait)
            
        if self.state == S1_wait:
            #if (utime.ticks_diff(self.current_time, self.next_check) >= 0):  #only continue if current_time has reached next_time
            if (utime.ticks_diff(utime.ticks_us(), self.next_check)) >= 0:
                
                


                if self.ser.any():
                    #char_in = self.ser.read(1).decode()
                    char_in = self.ser.read(1).decode()
                    
                
                    
                    if char_in == "z":
                        self.z_flag_1.write(True)
                        print("the position of encoder 1 will be zeroed") #in task_encoder print("task has been zeroed")
                    elif char_in == "Z":
                        self.z_flag_2.write(True)
                        print("the position of encoder 2 will be zeroed") #in task_encoder print("task has been zeroed")

                    
                        
                    elif char_in == "p":
                        print(self.pos_1.read())
                    elif char_in == "P":
                        print(self.pos_2.read())
    
                    elif char_in == "d":
                        print(self.del_1.read())
                    elif char_in == "D":
                        print(self.del_1.read())
                        
                    elif char_in == "g":
                        self.collectID = 1              #denotes collecting encoder 1 data
                        self.start_time = utime.ticks_us()
                        self.transition_to(S2_collect)
                    elif char_in == "G":
                        self.collectID = 2              #denotes collecting encoder 2 data
                        self.start_time = utime.ticks_us()
                        self.transition_to(S2_collect)  
                    
                    self.next_check = utime.ticks_add(self.next_check, self.period_check)
                    #CHECK IF 1 IN READ IS NEEDED 
            

        if self.state == S2_collect:
           
#Add functionallity to collect time, position (radians), delta (rad/s) in seperate collumns (use an array)
            
            if (utime.ticks_diff(utime.ticks_us(), self.next_data) >= 0):  #only continue if current_time has reached next_time
                
                
                if self.collectID == 1:
                    
                    
                    new_tuple = (utime.ticks_diff(utime.ticks_us(), self.start_time), self.pos_1.read(), self.del_1.read())
                    self.data.append(new_tuple)
                    
                    
                elif self.collectID == 2:

                    new_tuple = (utime.ticks_diff(utime.ticks_us(), self.start_time), self.pos_2.read(), self.del_2.read())
                    self.data.append(new_tuple)
                    
                    
                else:
                    raise ValueError("Invalid collect ID")
                    
                
           
                
                self.next_data = utime.ticks_add(self.next_data, self.period_data)
                    
                
                                  
#            else:
#                if self.collectID == 1:
#            
#                    new_tuple = (utime.ticks_diff(self.current_time, self.start_time), self.pos_1.read(), self.del_1.read())
#                    self.data.append(new_tuple)
#                    
#                elif self.collectID ==2:
#                    new_tuple = (utime.ticks_diff(self.current_time, self.start_time), self.pos_2.read(), self.del_2.read())
#                    self.data.append(new_tuple)
                    
            if (utime.ticks_diff(utime.ticks_us(), self.start_time) >= 5_000_000):
               print("Data collection complete")
               print("There are {:} data points".format(len(self.data)))
               self.transition_to(S3_print)        
                    
            if self.ser.any():
                char_in = self.ser.read(1).decode()
                if (char_in == 's' or char_in == 'S'):
                    self.transition_to(S3_print)
                    print("collection ended early")
                    print("There are {:} data points".format(len(self.data)))
                else:
                    print("To interrupt data collection press /'s/' or /'S/' or wait 30 seconds")             
                    
           

              
                    #check if there is user input
                        #read user input
                            #interpret user input
                                #action based on user input
                                    #transition to state 3
                    #check if 30 seconds have passed
                        #transition to state 3
                    #else collect data
                        #append current_time to [data array]
                        #read position
                            #append position to [data array]
                        #read delta
                            #append delta to [data array]


        if self.state == S3_print:
            
            if len(self.data) == 0:
                print("all data has been printed!")
                self.transition_to(S1_wait)
            else:
                print(self.data.pop(0))

                    
                    
                
                #check if values in [array]
                    #no value
                        #action for no value
                        #transition to state1
                    #value
                        #print value from array
                        #remove printed value from array
                
        
                
    def transition_to(self, new_state):
        """@brief          #put something here! <-------
        @detials        #put something here! <-------
        @param  new_state  the number or constant associated with the state to transition to
        """
        self.state = new_state
