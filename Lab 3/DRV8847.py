""" @file            #put something here! <-------
    @brief         #put something here! <-------
    @detials        #put something here! <-------
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/19/2021 #put something here! <-------

"""
import pyb
import time
class DRV8847:
    """ @brief         #put something here! <-------
        @detials        #put something here! <-------
    """
    
    def __init__(self, timNum, pinFault, pinSleep, fault_share, fault_interrupt): #what does fault_interrupt do?
        """ @brief
            @details
            @param pinA     Pin location for channel 1 (check this)
            @param pinB     Pin location for channel 2 (check this)
            @param timNum   Designates timer number
        """
        #timer
        self.tim = pyb.Timer(timNum, prescaler=0, period=20_000)
        
        self.pinSleep = pinSleep
        self.pinFault = pinFault
         
        self.pinSleep.high()    
        
        #motor channels   
        # self.tim_ch1 = self.tim.channel(1, pyb.Timer.PWM, pin=self.pinA)
        # self.tim_ch2 = self.tim.channel(2, pyb.Timer.PWM, pin=self.pinB)
       
        
    def enable (self):
        """ @brief         #put something here! <-------
        """
    pass

    def disable (self):
        """ @brief         #put something here! <-------
        """
        pass

    def fault_cb (self, IRQ_src):
        """ @brief         #put something here! <-------
        """
        
        
        pass
    
    def motor (self, chanA, chanB, pinA, pinB):
        """ @brief         #put something here! <-------
        """
        
        
         
        the_motor = Motor(chanA, chanB, pinA, pinB, self.tim)
        return the_motor
         
    
    
class Motor:
    """ @brief         #put something here! <-------
        @details        #put something here! <-------
    """
    def __init__ (self, chanA, chanB, pinA, pinB, tim):
         """ @brief         #put something here! <-------
             @details        #put something here! <-------
         """ 
         self.chA = tim.channel(chanA, pyb.Timer.PWM, pin = pinA)
         self.chB = tim.channel(chanB, pyb.Timer.PWM, pin = pinB)
         
     
    def set_duty (self, duty):
         """ @brief         #put something here! <-------
             @details        #put something here! <-------
         """
         self.duty = duty
        
         if self.duty > 0 and self.duty <= 100: 
             self.chA.pulse_width_percent(self.duty)
             self.chB.pulse_width_percent(0)
             
         elif self.duty >0 and self.duty >= -100:
            self.chA.pulse_width_percent(0)
            self.chB.pulse_width_percent(-1*self.duty)
            
         elif self.duty == 0:
            self.chA.pulse_width_percent(0)
            self.chB.pulse_width_percent(0)
            
         else:
            raise ValueError('Invalid entry. Please enter a number between -100 and 100')
             
     
if __name__ == '__main__':
    
    
    #motor_1 = Motor(1)
   # motor_1.set_duty(60)
    
    pinA = pyb.Pin(pyb.Pin.cpu.B4)
    pinB = pyb.Pin(pyb.Pin.cpu.B5)
    timNum = 3
    pinFault = pyb.Pin(pyb.Pin.cpu.B2)
    pinSleep = pyb.Pin(pyb.Pin.cpu.A15)
    fault_share = 0
    fault_interrupt = 0
    
     #def __init__(self, pinA, pinB, timNum, pinFault, pinSleep, fault_share, fault_interrupt):
    motor_drv_1   = DRV8847(timNum, pinFault, pinSleep, fault_share, fault_interrupt)
    motor_1     = motor_drv_1.motor(1, 2, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5)
    motor_2     = motor_drv_1.motor(3, 4, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1)
   
    print('i am a motor')
    motor_1.set_duty(60)
    
    time.sleep(5)
    #motor_1.set_duty(40)
    #motor_2.set_duty(60)

    
            