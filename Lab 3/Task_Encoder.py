""" @file           Task_Encoder.py
    @brief          Encoder task for utilizing Encoder class
    @details        Implements FSM associated with utilizing custom Encoder class. Updates position                             unless directed to reset position to zero. #not sure if this can be on this line
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/12/2021

"""

#import statements
import utime
from micropython import const

#set states
## State 0 of Encoder task
S0_init = const(0)
##State 1 of encoder task
S1_wait = const(1)

#make class
class Task_Encoder():
    ''' @brief      LED task for cooperative multitasking example.
        @details    Implements a finite state machine
    '''
    def __init__(self, period, encoder, position, delta, z_flag):
        ''' @brief              Constructs an Encoder task.
            @details            The Encoder task is implemented as a finite state machine.
            @param              period   Period, in microseconds, that data is taken from encoder.    
            @param              encoder  Designates which encoder is being used. 
            @param              position Position from encoder driver
            @param              delta    Delta from encoder driver.
            @param              z_flag   Share indicating if encoder must be zeroed. 
         '''
        self.period = period
        self.encoder = encoder
        self.pos = position
        self.delta = delta
        self.z_flag = z_flag
        
        self.current_time = 0

        self.next_time = utime.ticks_add(self.current_time, period)

            
    def run(self):
        ''' @brief      Runs the encoder task.
            @details    This will update the encoder data (position, time, delta).
        '''
        #print("encoder is running")
        self.current_time = utime.ticks_us()

        
        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.next_time = utime.ticks_add(self.next_time, self.period)
            
            
            if self.z_flag.read():
                self.encoder.set_position(0)
                self.pos.write(0)                                       #forces pos to be written zero, not written as pos
                #self.pos.write(self.pos)                               #writes new pos to share pos, check this!
                print("The position of the encoder has been zeroed")
                self.z_flag.write(False)
                
            
            self.encoder.update()
            self.pos.write(self.encoder.get_position())
            self.delta.write(self.encoder.get_delta())
            #print("stuff is updated")
            
            #time of data collection is not shared!!! this seems like a problem!!!
            #should we make tuples in here? 
            
        
        

        #update current time
        #if time to run
            #actually run dude
            
            
        