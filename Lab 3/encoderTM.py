""" @file           encoderTM.py
    @brief          A driver for reading from Quadrature Encoders
    @details        Includes general functions for use with Quadrature Encoders. (more here?)
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/12/2021

"""
#import section
import pyb

class Encoder:
    """ @brief       A driver for reading from Quadrature Encoders
        @details     Includes general functions for use with Quadrature Encoders. (more here?)
    """

    def __init__(self, pinA, pinB, timNum):
        """ @brief
            @details
            @param pinA     Pin location for channel 1 (check this)
            @param pinB     Pin location for channel 2 (check this)
            @param timNum   Designates timer number
        """
        #timer
        self.tim = pyb.Timer(timNum, prescaler=0, period=65535)
        
        #encoder channels   
        self.tim_ch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin=pinA)
        self.tim_ch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin=pinB)
        self.prev = 0
        self.pos = 0
        self.count = 0
        
        
    def update(self):
        """ @brief              Finds current encoder position
            @details            Update encoder position by determining distance traveled. Detects over/                                     under flow.
        """
        
        self.count = self.tim.counter()
        self.delta = self.count - self.prev        
        self.prev = self.count

        if self.delta > 65536/2:         #decreasing value/underflow
            self.delta = self.delta-65536
          
            
        elif self.delta < (-65536/2):     #increasing value/overflow          
            self.delta = self.delta+65536
        
        self.pos = self.pos + self.delta
        
        #print("stuff is updated")

    def get_position(self):
        """ @brief      Returns current encoder position
            @return     Current position of encoder
        """
        return self.pos


    def set_position(self, position):
        """ @brief                      Manually set position to provided value
            @details                    Manually set position to provided value, generally used to reset                                            or "home" position
            @param position             Home position, usually 0
        """
        print('setting position value')
        self.pos = position
        self.prev = self.tim.counter()
        self.detla = 0
        

    def get_delta(self):
        """ @brief                      Return change in position                  
            @return                     Return change in position
        """
        return self.delta


##for testing
#if __name__ == "__main__":
#    
#    pinPC6 = pyb.Pin (pyb.Pin.cpu.C6)
#    pinPC7 = pyb.Pin (pyb.Pin.cpu.C7)
#    encoder_1 = Encoder(pinPC6, pinPC7, 3)
#    
#    pinPB6 = pyb.Pin (pyb.Pin.cpu.B6)
#    pinPB7 = pyb.Pin (pyb.Pin.cpu.B7)
#    encoder_2 = Encoder(pinPB6, pinPB7, 4)
#
#    while True:
#        encoder_1.update()
#        print(encoder_1.pos)


            
        
    