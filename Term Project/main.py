''' @file       main.py
    @brief      Main file for a ball balancing platform.
    @details    Runs an IMU, touch panel, controller, data collection and user 
                task to balance a ball on a platform. 
                \image html Finaltaskdiagram.jpg
    @author     Tori Bornino
    @author     Jackson McLaughlin
    @author     Kendall Chappell
    @date       December 4, 2021
'''
import shares
import task_imu
import task_panel
import task_controller
import task_data_collect
import task_user

_IMU_PERIOD = 5000         # us = 200 Hz
_PANEL_PERIOD = 5000         # us = 200 Hz
_COLLECT_PERIOD = 5000    # us = 200 Hz
_CONTROLLER_PERIOD = 5000         # us = 200 Hz
_USER_PERIOD = 1000000         # us = 1 Hz

if __name__ == '__main__':
    _imu_share = shares.Share()
    _panel_share = shares.Share()
    _collectCMD = shares.Share(False)
    _balanceCMD = shares.Share(False)
    _motor_share = shares.Share()
    
    _imu_task = task_imu.Task_IMU(_IMU_PERIOD,  _imu_share)
    _panel_task = task_panel.Task_Panel(_PANEL_PERIOD, _panel_share)
    _controller_task = task_controller.Task_Controller(_CONTROLLER_PERIOD, _balanceCMD, _imu_share, _panel_share, _motor_share)
    _data_collect_task = task_data_collect.Task_Data_Collect(_COLLECT_PERIOD, _collectCMD, _imu_share, _panel_share, _motor_share)
    _user_task = task_user.Task_User(_USER_PERIOD, _collectCMD, _balanceCMD)

# Proposed way of calibration    
    _imu_task.calibrate()
    _panel_task.calibrate()


    _user_task.list_commands()
#    
    while True:
        try:
            _imu_task.run()              # Collect angle data
            _panel_task.run()            # Collect linear data
            _controller_task.run()       # Controller sets duty cycle from data
            _data_collect_task.run()     # Store data for printing
            _user_task.run()             # Check for user inputs
            
        except KeyboardInterrupt:
            #motor_drv.disable()
            print("exited")
            break
