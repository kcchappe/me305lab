''' @file       touchpanel.py
    @brief      A resistive touch panel driver
    @details    Contains the class TouchPanel
    @author     Tori Bornino
    @author     Jackson McLaughlin
    @author     Kendall Chappell
    @date       December 4, 2021
'''

import utime
from pyb import Pin, ADC
import micropython
from ulab import numpy as np

_MARGIN = micropython.const(20)
_IN = micropython.const(Pin.IN)
_OUT = micropython.const(Pin.OUT_PP)

class TouchPanel:
    ''' @brief      TouchPanel class for resistive touch panel
        @details    Allows a touch panel to read its x, y, and z (boolean) 
                    coordinate values separately, all three raw ADC readings,
                    all three converted to (x, y, z) using the calibration matrix, 
                    and all three coordinates and their velocities computed using 
                    an Alpha / Beta filter. Also, contains methods to compute 
                    the calibration matrix required to compute the three coordinates 
                    when given raw calibration data. Coordinates are read as
                    (x, y, z) in (mm, mm, boolan). Velocities are read as 
                    (xdot, ydot) in (mm/s, mm/s).
    '''
    def __init__(self, pinXP=Pin.cpu.A7, pinXM=Pin.cpu.A1, 
                 pinYP=Pin.cpu.A6, pinYM=Pin.cpu.A0,
                 width=176, length=100, centerX=176/2, centerY=100/2,
                 alpha=0.85, beta=0.005, *, t_s):
        ''' @brief      Initializes a touch panel object.
            @details    Assumes default pins and touch panel dimensions, 
                        and alpha / beta filter conditions.
            @param      pinXP       Positive x-axis pin, default PA7.
            @param      pinXM       Negative x-axis pin, default PA1.
            @param      pinYP       Positive y-axis pin, default PA6.
            @param      pinYM       Negative y-axis pin, default PA0.
            @param      width       Width of panel (along x) default 176 [mm].
            @param      length      Length of panel (along y) default 100 [mm]. 
            @param      centerX     Center of panel (along x) default 88 [mm].
            @param      centerY     Center of panel (along y) default 50 [mm].
            @param      alpha       Alpha / beta filter position gain. Must be 
                                    within 0 < alpha < 1 for stability. 
                                    Default value alpha = 0.85.
            @param      beta        Alpha / beta filter velocity gain. Must be 
                                    within 0 < beta < 1 for stability. 
                                    Default value beta = 0.005.
            @param      t_s         Alpha / beta filter sample time. 
        '''
        self._pinXP = pinXP
        self._pinXM = pinXM
        self._pinYP = pinYP
        self._pinYM = pinYM
        
        self._width = width
        self._length = length
        self._centerX = centerX
        self._centerY = centerY
        
        self._scaleX = self._width / 4095
        self._scaleY = self._length / 4095
        
        self._alpha = alpha
        self._beta = beta
        self._t_s = t_s
        
        self._x_last = 0
        self._y_last = 0
        self._vx_last = 0
        self._vy_last = 0
        
        self._actual_points = np.array([[-80, 40], [0, 40], [80, 40], [80, 0], [80, -40], [0, -40], [-80, -40], [-80, 0], [0, 0]])
    
    def scanX(self):
        ''' @brief      Scan X channel of touch panel.
            @details    Sets x positive channel to high and x negative channel 
                        to low. Then sets both y channels as input to measure 
                        the ADC voltage across the resistive touch panel. 
                        Then computes the position from the center using the 
                        assumed panel dimensions.  
            @return     X, position from center.
        '''
        self._pinXP.init(mode=_OUT, value=1)
        self._pinXM.init(mode=_OUT, value=0)
        self._pinYM.init(mode=_IN)
        self._pinYP.init(mode=_IN)
        utime.sleep_us(4)
        return ADC(self._pinYM).read() * self._scaleX - self._centerX
        
    
    def scanY(self):
        ''' @brief      Scan Y channel of touch panel.
            @details    Sets y positive channel to high and y negative channel 
                        to low. Then sets both x channels as input to measure 
                        the ADC voltage across the resistive touch panel. 
                        Then computes the position from the center using the 
                        assumed panel dimensions.  
            @return     Y, position from center.
        '''
        self._pinYP.init(mode=_OUT, value=1)
        self._pinYM.init(mode=_OUT, value=0)
        self._pinXM.init(mode=_IN)
        self._pinXP.init(mode=_IN)
        utime.sleep_us(4)
        return ADC(self._pinXM).read() * self._scaleY - self._centerY

    
    def scanZ(self):
        ''' @brief      Scan Z channel of touch panel.
            @details    Sets y positive channel to high and x negative channel 
                        to low. Then other two channels as input to measure 
                        the ADC voltage across the resistive touch panel. 
                        If the reading is either low or high, then the ball 
                        is not in contact with the panel and the boolean
                        false is returned. For any intermediate values, True 
                        is returned.
            @return     Z position (touched/untouched boolean).
        '''
        self._pinYP.init(mode=_OUT, value=1)
        self._pinXM.init(mode=_OUT, value=0)
        self._pinYM.init(mode=_IN)
        self._pinXP.init(mode=_IN)
        utime.sleep_us(4)
        return (0 + _MARGIN) < ADC(self._pinYM).read() < (4095 - _MARGIN)
    
    def scanAll(self):
        ''' @brief      Scan all (X,Y,Z) channels of touch panel and compute ball location.
            @details    In three stages the x, z, and y positions are read. 
                        First, set the x positive channel to high and x negative 
                        channel to low. Then sets both y channels as input to 
                        measure the ADCx voltage across the resistive touch panel. 
                        Then it sets the y positive channel to high and 
                        x positive channel to input to read the ADCz voltage and 
                        returns a boolean for if the ball is in contact with 
                        the plate. Then it sets the y negative channel to low
                        and the x negative channel to input to read the ADCy 
                        voltage across the resistive touch panel. Finally, the 
                        x and y positions on the board are calcualted using the 
                        calibration matrix.
            @return     Tuple of touch panel readings (x,y,z) (mm, mm, boolean).
        '''
        
        self._pinXP.init(mode=_OUT, value=1)
        self._pinXM.init(mode=_OUT, value=0)
        self._pinYM.init(mode=_IN)
        self._pinYP.init(mode=_IN)
        utime.sleep_us(4)
        ADC_x = ADC(self._pinYM).read()
        
        self._pinYP.init(mode=_OUT, value=1)
        self._pinXP.init(mode=_IN)
        utime.sleep_us(4)
        z = (0 + _MARGIN) < ADC(self._pinYM).read() < (4095 - _MARGIN)
        
        self._pinYM.init(mode=_OUT, value=0)
        self._pinXM.init(mode=_IN)
        utime.sleep_us(4)
        ADC_y = ADC(self._pinXM).read()

        ADC_matrix = np.array([[ADC_x, ADC_y, 1]])
        x, y = tuple(np.dot(ADC_matrix, self._calibMatrix).flatten())
        return x, y, z
    
    def scanAllRaw(self):
        ''' @brief      Scan all (X,Y,Z) channels of touch panel for raw ADC readings.
            @details    In three stages the x, z, and y positions are read. 
                        First, set the x positive channel to high and x negative 
                        channel to low. Then sets both y channels as input to 
                        measure the ADCx voltage across the resistive touch panel. 
                        Then it sets the y positive channel to high and 
                        x positive channel to input to read the ADCz voltage and 
                        returns a boolean for if the ball is in contact with 
                        the plate. Then it sets the y negative channel to low
                        and the x negative channel to input to read the ADCy 
                        voltage across the resistive touch panel. 
            @return     Tuple of touch panel readings (x,y,z) (V, V, boolean).
        '''
        self._pinXP.init(mode=_OUT, value=1)
        self._pinXM.init(mode=_OUT, value=0)
        self._pinYM.init(mode=_IN)
        self._pinYP.init(mode=_IN)
        utime.sleep_us(4)
        xRaw = ADC(self._pinYM).read()
        self._pinYP.init(mode=_OUT, value=1)
        self._pinXP.init(mode=_IN)
        utime.sleep_us(4)
        z = (0 + _MARGIN) < ADC(self._pinYM).read() < (4095 - _MARGIN)
        self._pinYM.init(mode=_OUT, value=0)
        self._pinXM.init(mode=_IN)
        utime.sleep_us(4)
        yRaw = ADC(self._pinXM).read()
        return (xRaw, yRaw, z)
    
    def get_AB_filtered(self):
        ''' @brief      Compute the filtered ball position and velocity. 
            @details    Uses scanAll() to read the ball's location then applies
                        an alpha / beta filter to compute the estimated ball 
                        position and velocity.
            @return     Filtered ball position and velocity (x, xdot, y, ydot)
                        in (mm, mm/s, mm, mm/s).
        
        '''
        x, y, z = self.scanAll()
        if z:
            x_est = self._x_last + self._alpha * (x - self._x_last) + self._t_s * self._vx_last
            y_est = self._y_last + self._alpha * (y - self._y_last) + self._t_s * self._vy_last
            vx_est = self._vx_last + self._beta / self._t_s * (x - self._x_last)
            vy_est = self._vy_last + self._beta / self._t_s * (y - self._y_last)
            self._x_last = x_est
            self._y_last = y_est
            self._vx_last = vx_est
            self._vy_last = vy_est
            return (x_est, vx_est, y_est, vy_est, z)
        else: # no contact = bad data
            return (0, 0, 0, 0, z)
        
    
    def calibrate(self, read_points):
        ''' @brief      Calculate and store the calibration matrix for the touch panel.
            @details    Computes and stores the calibration matrix for the 
                        touch panel within the touch panel object based on the 
                        inputed raw ADC readings and the idealized locations 
                        that were touched on the touch panel. 
            @param      read_points     Nby2 matrix of raw ADC readings for the
                                        x and y coordinates touched during 
                                        calibration.
            @return     tuple of flattened calibration matrix 
                        (Kxx, Kxy, Kyx, Kyy, Xc, Yc).
        '''
        # Tap points
        
        calibMatrix = np.dot(np.dot(np.linalg.inv(np.dot(read_points.T, read_points)), read_points.T), self._actual_points)
        self._calibMatrix = calibMatrix
        return tuple(calibMatrix.flatten())
    
    def set_calibration(self, calib_list):
        ''' @brief      Store the calibration matrix for the touch panel.
            @details    Store given calibration data into the object's 
                        calibration matrix.
            @param      calib_list      List of calibration data of the form
                                        (Kxx, Kxy, Kyx, Kyy, Xc, Yc).
        '''
        self._calibMatrix = np.array(calib_list).reshape((3, 2))
    

